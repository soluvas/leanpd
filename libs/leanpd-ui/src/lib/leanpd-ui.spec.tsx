import { render } from '@testing-library/react';

import LeanpdUi from './leanpd-ui';

describe('LeanpdUi', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<LeanpdUi />);
    expect(baseElement).toBeTruthy();
  });
});
