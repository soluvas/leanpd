# Use Puppeteer (headless Chromium browser)
# From: https://github.com/qahive/robotframework-puppeteer/blob/master/.gitpod.Dockerfile
FROM gitpod/workspace-full
RUN sudo apt-get update \
 && sudo DEBIAN_FRONTEND=noninteractive apt-get install -y \
   libgtk2.0-0 \
   libgtk-3-0 \
   libnotify-dev \
   libgconf-2-4 \
   libnss3 \
   libxss1 \
   libasound2 \
   libxtst6 \
   xauth \
   xvfb \
 && sudo rm -rf /var/lib/apt/lists/*
