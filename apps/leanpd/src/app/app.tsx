import {
  Routes,
  Route,
  Link,
  BrowserRouter,
  useSearchParams,
} from 'react-router-dom';
// const StyledApp = styled.div`
//   // Your style here
// `;
import { LeanRoadmap, LeanRoadmapProps } from '@soluvas/leanpd-ui';
import { useEffect, useState } from 'react';
import axios from 'axios';

export function Routed() {
  const [searchParams] = useSearchParams();
  const path = searchParams.get('path');
  const [roadmapProps, setRoadmapProps] = useState<LeanRoadmapProps>();
  useEffect(() => {
    const doIt = async (path: string) => {
      const res = await axios.get(path);
      console.info('result:', res);
      setRoadmapProps(res.data);
    };

    console.log('Fetching', path, '...');
    if (path) {
      doIt(path);
    } else {
      throw new Error('path cannot be empty');
    }
  }, []);
  return roadmapProps ? <LeanRoadmap {...roadmapProps} /> : <>Loading...</>;
}

export function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Routed />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
