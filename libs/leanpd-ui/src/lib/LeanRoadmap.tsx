import React from 'react';
import { Icon } from '@iconify/react';
import { Box, Stack, Typography } from '@mui/material';
import dayjs from 'dayjs';
import duration from 'dayjs/plugin/duration';
import { motion } from 'framer-motion';
dayjs.extend(duration);

export enum LeanRoadmapKind {
  Portfolio = 'PORTFOLIO',
  Solution = 'SOLUTION',
  Program = 'PROGRAM',
}

export enum LeanMilestoneKind {
  Generic = 'GENERIC',
  Release = 'RELEASE',
}

export enum LeanMilestonePeriod {
  Day = 'DAY',
  Quarter = 'QUARTER',
  Half = 'HALF',
  Year = 'YEAR',
}

export interface LeanMilestone {
  title: string;
  period: LeanMilestonePeriod;
  year?: number;
  month?: number;
  quarter?: number;
  half?: number;
  day?: string;
  kind: LeanMilestoneKind;
  /**
   * Visual workaround when multiple milestones jumble together.
   */
  staggered?: boolean;
}

export enum LeanFeatureKind {
  /**
   * Used to denote no work.
   */
  Empty = 'EMPTY',
  Enabler = 'ENABLER',
  Business = 'BUSINESS',
}

export interface LeanFeature {
  title: string;
  kind: LeanFeatureKind;
  /**
   * ISO 8601 durations, see https://day.js.org/docs/en/durations/creating
   */
  duration: string;
}

/**
 * A "channel" is a proxy construct: In case of Portfolio Roadmap, it is Solutions.
 * In a Solution Roadmap, it is Programs.
 * In a Program Roadmap, it is Teams.
 */
export interface LeanChannel {
  id: string;
  title: string;
  features: LeanFeature[];
}

export enum LeanRoadmapRange {
  /**
   * 3 full PIs, instead of six months which only contain 26 weeks.
   */
  ThirtyWeeks = 'THIRTY_WEEKS',
  OneYear = 'ONE_YEAR',
  TwoYears = 'TWO_YEARS',
  ThreeYears = 'THREE_YEARS',
}

export interface LeanRoadmapProps {
  range: LeanRoadmapRange;
  kind: LeanRoadmapKind;
  milestones: LeanMilestone[];
  channels: LeanChannel[];
}

export function LeanRoadmap({
  range,
  kind,
  milestones,
  channels,
}: LeanRoadmapProps) {
  let minPeriod = '';
  let maxPeriod = '';
  let ticks: { title: string; unix: number }[] = [];
  switch (range) {
    case LeanRoadmapRange.ThirtyWeeks:
      minPeriod = '2022-01-01';
      maxPeriod = '2023-01-01';
      ticks = [
        {
          title: '2022-Q1',
          unix: dayjs('2022-01-01').unix(),
        },
        {
          title: '2022-Q2',
          unix: dayjs('2022-04-01').unix(),
        },
        {
          title: '2022-Q3',
          unix: dayjs('2022-07-01').unix(),
        },
        {
          title: '2022-Q4',
          unix: dayjs('2022-10-01').unix(),
        },
        {
          title: '2023-H1',
          unix: dayjs('2023-01-01').unix(),
        },
        {
          title: '2023-H2',
          unix: dayjs('2023-07-01').unix(),
        },
        {
          title: '2024',
          unix: dayjs('2024-01-01').unix(),
        },
        {
          title: '2025',
          unix: dayjs('2025-01-01').unix(),
        },
      ];
      break;
    case LeanRoadmapRange.OneYear:
      minPeriod = '2022-01-01';
      maxPeriod = '2023-01-01';
      ticks = [
        {
          title: '2022-Q1',
          unix: dayjs('2022-01-01').unix(),
        },
        {
          title: '2022-Q2',
          unix: dayjs('2022-04-01').unix(),
        },
        {
          title: '2022-Q3',
          unix: dayjs('2022-07-01').unix(),
        },
        {
          title: '2022-Q4',
          unix: dayjs('2022-10-01').unix(),
        },
        {
          title: '2023',
          unix: dayjs('2023-01-01').unix(),
        },
      ];
      break;
    case LeanRoadmapRange.TwoYears:
      minPeriod = '2022-01-01';
      maxPeriod = '2024-01-01';
      ticks = [
        {
          title: '2022-Q1',
          unix: dayjs('2022-01-01').unix(),
        },
        {
          title: '2022-Q2',
          unix: dayjs('2022-04-01').unix(),
        },
        {
          title: '2022-Q3',
          unix: dayjs('2022-07-01').unix(),
        },
        {
          title: '2022-Q4',
          unix: dayjs('2022-10-01').unix(),
        },
        {
          title: '2023-H1',
          unix: dayjs('2023-01-01').unix(),
        },
        {
          title: '2023-H2',
          unix: dayjs('2023-07-01').unix(),
        },
        {
          title: '2024',
          unix: dayjs('2024-01-01').unix(),
        },
        {
          title: '2025',
          unix: dayjs('2025-01-01').unix(),
        },
      ];
      break;
    case LeanRoadmapRange.ThreeYears:
    default:
      minPeriod = '2022-01-01';
      maxPeriod = '2025-01-01';
      ticks = [
        {
          title: '2022-Q1',
          unix: dayjs('2022-01-01').unix(),
        },
        {
          title: '2022-Q2',
          unix: dayjs('2022-04-01').unix(),
        },
        {
          title: '2022-Q3',
          unix: dayjs('2022-07-01').unix(),
        },
        {
          title: '2022-Q4',
          unix: dayjs('2022-10-01').unix(),
        },
        {
          title: '2023-H1',
          unix: dayjs('2023-01-01').unix(),
        },
        {
          title: '2023-H2',
          unix: dayjs('2023-07-01').unix(),
        },
        {
          title: '2024',
          unix: dayjs('2024-01-01').unix(),
        },
        {
          title: '2025',
          unix: dayjs('2025-01-01').unix(),
        },
      ];
      break;
  }

  const svgWidth = 1600;
  const timelineLeft = 150;
  const timelineRight = 50;
  const timelineWidth = svgWidth - timelineLeft - timelineRight;

  const minUnix = dayjs(minPeriod).unix();
  const maxUnix = dayjs(maxPeriod).unix();
  const rangeUnix = maxUnix - minUnix;

  const milestoneInfos = milestones.map((_) => {
    const timeFrac = (dayjs(_.day).unix() - minUnix) / rangeUnix;
    return { ..._, timeFrac };
  });
  const channelHeight = 60;
  const channelTop = 80;
  const channelExtras = channels.map((channel, channelIdx) => {
    let lastX = timelineLeft;
    const featureExtras = channel.features.map((feature) => {
      const durationPx =
        (dayjs.duration(feature.duration).asSeconds() / rangeUnix) *
        timelineWidth;
      const left = lastX;
      const width = durationPx - 8; // space for a bit of margin
      const center = left + width / 2;
      lastX += durationPx;
      return { ...feature, left, width, center };
    });
    const top = channelTop + channelIdx * channelHeight;
    const middle = top + channelHeight / 2;
    return {
      ...channel,
      height: channelHeight - 8, // space for a bit of margin
      features: featureExtras,
      top,
      middle,
    };
  });
  const svgHeight =
    channelExtras[channelExtras.length - 1].top +
    channelExtras[channelExtras.length - 1].height +
    40;
  const todayX =
    timelineLeft + ((dayjs().unix() - minUnix) / rangeUnix) * timelineWidth;
  return (
    <Stack>
      <Box
        sx={{
          position: 'relative',
          height: '5rem',
          p: 3,
        }}
      >
        {milestoneInfos.map((milestone, idx) => (
          <motion.div
            key={milestone.title}
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            transition={{ delay: idx * 0.1, duration: 1.5 }}
          >
            <Stack
              key={milestone.title}
              alignItems="center"
              sx={{
                maxWidth: '8rem',
                position: 'absolute',
                left: milestone.timeFrac * 100 + '%',
                bottom: 0,
              }}
            >
              <Typography
                align="center"
                sx={{
                  mb: milestone.staggered ? 5 : 1,
                  fontFamily: 'Open Sans',
                  lineHeight: '120%',
                  fontSize: '70%',
                  color: '#333',
                }}
              >
                {milestone.title}
              </Typography>
              {milestone.kind === LeanMilestoneKind.Generic && (
                <Icon
                  icon="icon-park-twotone:diamond-three"
                  width="1.5em"
                  height="1.5em"
                />
              )}
              {milestone.kind === LeanMilestoneKind.Release && (
                <Icon icon="emojione:package" width="1.5em" height="1.5em" />
              )}
            </Stack>
          </motion.div>
        ))}
      </Box>
      <svg style={{}} viewBox={`0 0 ${svgWidth} ${svgHeight}`}>
        <style>
          @import
          url('https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap');
        </style>
        <motion.rect
          initial={{ width: 0 }}
          animate={{ width: timelineWidth }}
          transition={{ duration: 1.5 }}
          x={timelineLeft}
          y="50"
          width={timelineWidth}
          height="4"
          style={{ fill: '#596ca4' }}
        />
        {ticks.map((_, idx) => (
          <motion.text
            key={_.title}
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            transition={{ delay: idx * 0.1, duration: 1.5 }}
            y={30}
            x={timelineLeft + ((_.unix - minUnix) / rangeUnix) * timelineWidth}
            fill="#333"
            fontFamily="'Open Sans'"
            fontSize="120%"
            textAnchor="middle"
          >
            {_.title}
          </motion.text>
        ))}
        {ticks.map((_, idx) => (
          <motion.line
            key={_.title}
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            transition={{ delay: idx * 0.1, duration: 0.5 }}
            y1={40}
            y2={60}
            x1={timelineLeft + ((_.unix - minUnix) / rangeUnix) * timelineWidth}
            x2={timelineLeft + ((_.unix - minUnix) / rangeUnix) * timelineWidth}
            stroke="#596ca4"
            strokeWidth={4}
          />
        ))}
        {channelExtras.map((c) =>
          c.features.map((f, fIdx) =>
            f.kind !== LeanFeatureKind.Empty ? (
              <motion.g
                key={f.title}
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                transition={{ delay: fIdx * 0.3, duration: 1.5 }}
              >
                <rect
                  y={c.top}
                  x={f.left}
                  width={f.width}
                  height={c.height}
                  fill={
                    f.kind === LeanFeatureKind.Enabler ? '#a33b40' : '#596ca4'
                  }
                />
                <text
                  y={c.middle}
                  x={f.center}
                  fill="white"
                  fontSize="90%"
                  fontFamily="Open Sans"
                  textAnchor="middle"
                >
                  {f.title}
                </text>
              </motion.g>
            ) : undefined
          )
        )}
        {channelExtras.map((c, idx) => (
          <motion.text
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            transition={{ delay: idx * 0.1, duration: 0.6 }}
            textAnchor="end"
            key={c.id}
            y={c.middle}
            x={timelineLeft - 20}
            fill="#333"
            fontSize="100%"
            fontFamily="Open Sans"
            fontWeight="bold"
          >
            {c.title}
          </motion.text>
        ))}
        <motion.line
          initial={{ y1: svgHeight / 2, y2: svgHeight / 2 }}
          animate={{ y1: 0, y2: svgHeight }}
          transition={{ duration: 0.5 }}
          opacity={0.5}
          x1={todayX}
          x2={todayX}
          stroke="red"
          strokeWidth={4}
          y1={0}
          y2={svgHeight}
        />
      </svg>
    </Stack>
  );
}
