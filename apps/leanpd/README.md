## Visualize a Roadmap JSON file

After running `yarn nx serve leanpd`

Open in browser:

https://localhost:4200/?path=/assets/lean-roadmap/example1_solution.json
