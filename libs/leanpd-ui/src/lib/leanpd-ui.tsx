import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface LeanpdUiProps {}

const StyledLeanpdUi = styled.div`
  color: pink;
`;

export function LeanpdUi(props: LeanpdUiProps) {
  return (
    <StyledLeanpdUi>
      <h1>Welcome to LeanpdUi!</h1>
    </StyledLeanpdUi>
  );
}

export default LeanpdUi;
