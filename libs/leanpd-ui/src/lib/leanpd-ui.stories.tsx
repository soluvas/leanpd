import { ComponentStory, ComponentMeta } from '@storybook/react';
import { LeanpdUi } from './leanpd-ui';

export default {
  component: LeanpdUi,
  title: 'LeanpdUi',
} as ComponentMeta<typeof LeanpdUi>;

const Template: ComponentStory<typeof LeanpdUi> = (args) => (
  <LeanpdUi {...args} />
);

export const Primary = Template.bind({});
Primary.args = {};
