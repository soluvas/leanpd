import { ComponentStory, ComponentMeta } from '@storybook/react';
import { NxWelcomeLib } from './nx-welcome-lib';

export default {
  component: NxWelcomeLib,
  title: 'NxWelcome Lib',
} as ComponentMeta<typeof NxWelcomeLib>;

const Template: ComponentStory<typeof NxWelcomeLib> = (args) => (
  <NxWelcomeLib {...args} />
);

export const Primary = Template.bind({});
Primary.args = {};
