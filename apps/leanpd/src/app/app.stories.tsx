import { ComponentStory, ComponentMeta } from '@storybook/react';
import { sum } from '../../../../libs/leanpd-ui/src/lib/testutil';
import { App } from './app';

export default {
  component: App,
  title: 'App',
} as ComponentMeta<typeof App>;

const Template: ComponentStory<typeof App> = (args) => <App {...args} />;

export const Primary = Template.bind({});
Primary.args = {};

console.log('sum=', sum(5, 4));
