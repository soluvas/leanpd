import puppeteer from 'puppeteer';

async function sleep(ms: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

async function exportPng() {
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();
  const url =
    'http://localhost:4200?path=/assets/lean-roadmap/example1_solution.json';
  console.log('Opening page', url, '...');
  await page.goto(url, {
    waitUntil: 'networkidle0',
  });
  // wait for animations
  await sleep(2000);
  console.log('Exporting element...');
  // const pdf = await page.pdf({ format: 'A4' });

  const rootDiv = await page.$('#root');
  const png = await rootDiv.screenshot({
    path: '/workspace/leanpd-export.png',
  });
  console.log('Exported.');

  await browser.close();
  return png;
}

exportPng();
