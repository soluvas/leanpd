import { ComponentStory, ComponentMeta } from '@storybook/react';
import {
  LeanFeatureKind,
  LeanMilestoneKind,
  LeanMilestonePeriod,
  LeanRoadmap,
  LeanRoadmapKind,
  LeanRoadmapRange,
} from './LeanRoadmap';

export default {
  component: LeanRoadmap,
  title: 'Lean Roadmap',
} as ComponentMeta<typeof LeanRoadmap>;

const Template: ComponentStory<typeof LeanRoadmap> = (args) => (
  <LeanRoadmap {...args} />
);

export const Solution = Template.bind({});
Solution.args = {
  kind: LeanRoadmapKind.Portfolio,
  range: LeanRoadmapRange.ThreeYears,
  milestones: [
    {
      title: 'Autonomous platform integration with vehicle',
      period: LeanMilestonePeriod.Day,
      day: '2022-04-02',
      kind: LeanMilestoneKind.Generic,
    },
    {
      title: 'Route following and lane keeping v1.0',
      period: LeanMilestonePeriod.Day,
      day: '2022-07-04',
      kind: LeanMilestoneKind.Generic,
      staggered: true,
    },
    {
      title: 'First field test',
      period: LeanMilestonePeriod.Day,
      day: '2023-01-10',
      kind: LeanMilestoneKind.Generic,
    },
    {
      title: 'On-road testing certification',
      period: LeanMilestonePeriod.Day,
      day: '2024-01-10',
      kind: LeanMilestoneKind.Generic,
    },
    {
      title: 'Terrific Transport Delivery Vehicle v1.0',
      period: LeanMilestonePeriod.Day,
      day: '2024-07-15',
      kind: LeanMilestoneKind.Release,
    },
  ],
  channels: [
    {
      id: 'gps',
      title: 'GPS',
      features: [
        {
          kind: LeanFeatureKind.Enabler,
          title: 'GPS',
          duration: 'P3M',
        },
        {
          kind: LeanFeatureKind.Business,
          title: 'Follow route',
          duration: 'P5M',
        },
        {
          kind: LeanFeatureKind.Business,
          title: 'Make turns',
          duration: 'P4M',
        },
        {
          kind: LeanFeatureKind.Enabler,
          title: 'Perform Delivery Spike',
          duration: 'P8M',
        },
        {
          kind: LeanFeatureKind.Business,
          title: 'Perform delivery tests',
          duration: 'P6M',
        },
      ],
    },
    {
      id: 'vision',
      title: 'Vision',
      features: [
        {
          kind: LeanFeatureKind.Enabler,
          title: 'Surround cameras',
          duration: 'P5M',
        },
        {
          kind: LeanFeatureKind.Business,
          title: 'Stay in lane',
          duration: 'P5M',
        },
        {
          kind: LeanFeatureKind.Business,
          title: 'Obey traffic laws',
          duration: 'P5M',
        },
        {
          kind: LeanFeatureKind.Business,
          title: 'Avoid obstacles',
          duration: 'P4M',
        },
      ],
    },
    {
      id: 'radar',
      title: 'Radar',
      features: [
        {
          kind: LeanFeatureKind.Enabler,
          title: 'Forward radar',
          duration: 'P4M',
        },
        {
          kind: LeanFeatureKind.Business,
          title: 'Forward collision',
          duration: 'P5M',
        },
        {
          kind: LeanFeatureKind.Enabler,
          title: 'Ultrasonic sensors',
          duration: 'P4M',
        },
        {
          kind: LeanFeatureKind.Business,
          title: 'Go thru intersection',
          duration: 'P7M',
        },
        {
          kind: LeanFeatureKind.Business,
          title: 'Self-park',
          duration: 'P5M',
        },
      ],
    },
  ],
};
